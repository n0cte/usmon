CI_COMMIT_SHA?=local
LDFLAGS=-ldflags "-w -s -X main.Version=${CI_COMMIT_SHA}"

all: clean linux darwin windows

clean:
	if [ -d "build" ]; then rm -rf "build"; fi

linux:
	if [ ! -d "build" ]; then mkdir "build"; fi
	GOOS=linux GOARCH=amd64 go build ${LDFLAGS} -o build/dhub-linux-${CI_COMMIT_SHA}

darwin:
	if [ ! -d "build" ]; then mkdir "build"; fi
	GOOS=darwin GOARCH=amd64 go build ${LDFLAGS} -o build/dhub-darwin-${CI_COMMIT_SHA}

windows:
	if [ ! -d "build" ]; then mkdir "build"; fi
	GOOS=windows GOARCH=amd64 go build ${LDFLAGS} -o build/dhub-windows-${CI_COMMIT_SHA}