module gitlab.com/n0cte/usmon

go 1.15

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/ethereum/go-ethereum v1.9.25
	github.com/sirupsen/logrus v1.7.0
	github.com/urfave/cli/v2 v2.3.0
)
