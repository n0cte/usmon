package monitor

import (
	"bytes"
	"context"
	"fmt"
	"time"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/rpc"
	"github.com/sirupsen/logrus"
	"gitlab.com/n0cte/usmon/pkg/uniswap"
	"gitlab.com/n0cte/usmon/pkg/utils"
)

var log = logrus.WithField("module", "monitor")

// Monitor lookup new transactions to uniswap
type Monitor struct {
	jsonrpc     *rpc.Client
	uniswapAddr common.Address
	txCh        chan *RPCTransaction
}

// New create new object
func New(opts *Options) *Monitor {
	monitor := Monitor{
		jsonrpc:     opts.RPCClient,
		uniswapAddr: opts.UniswapAddress,
		txCh:        make(chan *RPCTransaction, 1),
	}
	utils.SetInterval(monitor.lookup, opts.LookupDelay, false)
	return &monitor
}

func (mon *Monitor) lookup() {
	defer utils.TimeTrack(time.Now())

	// [pending|queued] => [address] => [nonce] => {transaction}
	content := map[string]map[string]map[string]*RPCTransaction{
		"pending": make(map[string]map[string]*RPCTransaction),
		"queued":  make(map[string]map[string]*RPCTransaction),
	}
	if err := mon.jsonrpc.Call(&content, "txpool_content"); err != nil {
		logrus.Error(err)
		return
	}

	for channel := range content {
		for address := range content[channel] {
			for nonce := range content[channel][address] {
				tx := content[channel][address][nonce]
				if tx.To != nil && bytes.Compare(mon.uniswapAddr.Bytes(), tx.To.Bytes()) == 0 {
					mon.txCh <- tx
				}
			}
		}
	}
}

// Start calculate transaction to uniswap
func (mon *Monitor) Start(ctx context.Context, methodName string) error {
	method, exist := uniswap.PairABI.Methods[methodName]
	if !exist {
		return fmt.Errorf("'%s' doesn't exist", methodName)
	}

	for tx := range mon.txCh {
		l := log.WithField("hash", tx.Hash.Hex())
		l.Trace("transaction to uniswap")

		if len(tx.Input) < 4 {
			continue
		}

		sigdata, argdata := tx.Input[:4], tx.Input[4:]
		if bytes.Compare(method.ID, sigdata) != 0 {
			continue
		}

		args, err := method.Inputs.Unpack(argdata)
		if err != nil {
			l.WithField("method", method.Name).
				WithField("data", common.Bytes2Hex(argdata)).
				WithError(err).
				Error("can't decode inputs")
			continue
		}

		l.WithField("method", method.Name).Infof("args %#v", args...)
	}
	return nil
}
