package path

import (
	"os/user"
	"path/filepath"
	"strings"
)

// Clean replace "~" to HomeDir
func Clean(path string) (string, error) {
	path = filepath.Clean(path)
	if strings.HasPrefix(path, "~") {
		usr, err := user.Current()
		if err != nil {
			return "", err
		}
		path = filepath.Join(usr.HomeDir, path[1:])
	}
	return path, nil
}
