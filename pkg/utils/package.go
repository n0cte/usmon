package utils

import (
	"regexp"
	"runtime"
	"time"

	"github.com/sirupsen/logrus"
)

// SetInterval аналог функции из js
//   fn    - исполняемая функция
//   ms    - время между тиками, в миллисекундах
//   async - обозначает блокировку на период исполнения
// Возвращает канал, через который можно остановить таймер
func SetInterval(fn func(), interval time.Duration, async bool) chan bool {
	ticker := time.NewTicker(interval)
	clear := make(chan bool)

	go func() {
		for {
			select {
			case <-ticker.C:
				if async {
					go fn()
				} else {
					fn()
				}
			case <-clear:
				ticker.Stop()
				return
			}
		}
	}()

	return clear
}

// TimeTrack measures execution time
func TimeTrack(start time.Time) {
	elapsed := time.Since(start)

	// Skip this function, and fetch the PC and file for its parent.
	pc, _, _, _ := runtime.Caller(1)

	// Retrieve a function object this functions parent.
	funcObj := runtime.FuncForPC(pc)

	// Regex to extract just the function name (and not the module path).
	runtimeFunc := regexp.MustCompile(`^.*\.(.*)$`)
	name := runtimeFunc.ReplaceAllString(funcObj.Name(), "$1")

	logrus.WithField("function", name).Tracef("took %s", elapsed)
}
