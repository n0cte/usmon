package main

import (
	"context"
	"fmt"
	"os"
	"time"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/rpc"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/n0cte/usmon/pkg/monitor"
	"gitlab.com/n0cte/usmon/pkg/utils/path"
)

var (
	// Version is application version,
	// which is defined at build
	Version string

	// When launched in developer mode, set to true.
	// You can start in developer mode in three ways:
	// - watcher . (github.com/canthefason/go-watcher)
	// - go run main.go
	// - make
	isLocal = Version == "" || Version == "local"

	// Console application framework
	// contains subcommands and flags
	app *cli.App

	// list of application global flags
	flagLogLevel = &cli.StringFlag{
		Name:  "log.level",
		Value: "info",
	}
	flagLogFile = &cli.StringFlag{
		Name:  "log.file",
		Value: "",
	}
	flagLogFullTimestamp = &cli.BoolFlag{
		Name:  "log.fulltimestamp",
		Value: true,
	}
	flagLogDisableTimestamp = &cli.BoolFlag{
		Name:  "log.disabletimestamp",
		Value: false,
	}
	flagEthRPC = &cli.StringFlag{
		Name:  "ethrpc",
		Value: "http://localhost:8545",
	}
	flagUniswapAddr = &cli.StringFlag{
		Name:  "uniswap.address",
		Value: "0x9c83dCE8CA20E9aAF9D3efc003b2ea62aBC08351",
	}
	flagUniswapMethod = &cli.StringFlag{
		Name:  "uniswap.method",
		Value: "swap",
	}
	flagUniswapLookupDelay = &cli.StringFlag{
		Name:  "uniswap.sleep",
		Value: "300ms",
	}
)

func init() {
	app = &cli.App{
		Name:            fmt.Sprintf("usmon (%s)", Version),
		Usage:           "",
		UsageText:       "usmon [global options]",
		HideHelpCommand: true,
		Version:         Version,
		Before:          before,
		Action:          action,
		Flags: []cli.Flag{
			flagLogLevel,
			flagLogFile,
			flagLogFullTimestamp,
			flagLogDisableTimestamp,
			flagEthRPC,
			flagUniswapAddr,
			flagUniswapMethod,
			flagUniswapLookupDelay,
		},
	}
	if isLocal {
		app.Name = "usmon (local)"
	}
}

func before(ctx *cli.Context) error {
	logrus.SetFormatter(&logrus.TextFormatter{
		FullTimestamp:    ctx.Bool("log.fulltimestamp"),
		DisableTimestamp: ctx.Bool("log.disabletimestamp"),
	})

	loglevel, err := logrus.ParseLevel(ctx.String("log.level"))
	if err != nil {
		return err
	}
	logrus.SetLevel(loglevel)

	if logfile := ctx.String("log.file"); logfile != "" {
		lfile, err := path.Clean(logfile)
		if err != nil {
			return err
		}
		file, err := os.OpenFile(lfile, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
		if err != nil {
			return err
		}
		defer file.Close()
		logrus.SetOutput(file)
	}

	return nil
}

func action(ctx *cli.Context) error {
	jsonrpc, err := rpc.Dial(ctx.String("ethrpc"))
	if err != nil {
		return err
	}
	defer jsonrpc.Close()

	lookupDelay, err := time.ParseDuration(ctx.String("uniswap.sleep"))
	if err != nil {
		return err
	}

	mon := monitor.New(&monitor.Options{
		RPCClient:      jsonrpc,
		UniswapAddress: common.HexToAddress(ctx.String("uniswap.address")),
		LookupDelay:    lookupDelay,
	})

	return mon.Start(context.Background(), ctx.String("uniswap.method"))
}

func main() {
	if err := app.Run(os.Args); err != nil {
		logrus.Fatal(err)
	}
}
