package uniswap

import (
	"strings"

	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/sirupsen/logrus"
)

// PairABI uniswap pair interface
// https://unpkg.com/@uniswap/v2-core@1.0.0/build/IUniswapV2Factory.json
var PairABI abi.ABI

func init() {
	var err error
	PairABI, err = abi.JSON(strings.NewReader(pairJSON))
	if err != nil {
		logrus.Fatal(err)
	}
}
